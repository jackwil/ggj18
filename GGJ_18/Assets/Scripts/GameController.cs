﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public enum gameState
	{
		paused,
		active,
		gameOver,
		gameWon
	}
	public gameState currentGameState;

	public GameObject winScreenUI;
	public GameObject loseScreenUI;

	[Header ("Do not fill manually")]
	public BreadcrumbController breadcrumbController;

	bool endGameState;

	// Use this for initialization
	void Start () {
		currentGameState = gameState.active;

		breadcrumbController = BreadcrumbController.FindObjectOfType (typeof(BreadcrumbController)) as BreadcrumbController;
	}

//	void Update()
//	{
//		if (Input.GetKey (KeyCode.Keypad0))
//			StartCoroutine (NextLevel ());
//	}

	public void PlayerDefeated() // Called from MainCharController
	{
		if (!endGameState) {
			currentGameState = gameState.gameOver;
			loseScreenUI.SetActive (true);
			endGameState = true;
			StartCoroutine (RestartLevel ());
		}
	}

	public void PlayerVictory() // Called from Radio Tower
	{
		if (!endGameState) {
			currentGameState = gameState.gameWon;
			winScreenUI.SetActive (true);
			endGameState = true;
			StartCoroutine (NextLevel ());
		}
	}

	IEnumerator RestartLevel()
	{
		yield return new WaitForSecondsRealtime (3);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	IEnumerator NextLevel()
	{
		yield return new WaitForSecondsRealtime (3);

		int nextScene = SceneManager.GetActiveScene ().buildIndex + 1;


		if (nextScene == SceneManager.sceneCountInBuildSettings)
			nextScene = 0;

		SceneManager.LoadScene (nextScene);
	}

}
