﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {

	public AudioSource audioSourceSFX;
	public AudioSource audioSourceSFX2;
	public AudioSource audioSourceUploadSFX;
	public AudioSource audioSourcePlayerSFX;
	public AudioSource audioSourceMusic;

	[Space (20)]
	public AudioClip tenseMusic;
	public AudioClip actionMusic;

	[Space (20)]
	public AudioClip useSonarSFX;
	public AudioClip uploadRadioSFX;
	public AudioClip sonarOverchargeSFX;
	public AudioClip bushRustleSFX;
	public AudioClip footStepsSFX;
	public AudioClip enemyDiscoveredSFX;
	public AudioClip playerLostSFX;

	bool playerMoving;
	bool radioUploading;

	// Use this for initialization
	void Start () {
		audioSourceSFX.loop = false;
		audioSourceMusic.clip = tenseMusic;
		audioSourceMusic.Play ();
	}
	
	public void PlaySFX(AudioClip sfx)
	{
		if (!audioSourceSFX.isPlaying)
		{
			audioSourceSFX.clip = sfx;
			audioSourceSFX.Play ();
		} else if (!audioSourceSFX2.isPlaying)
		{
			audioSourceSFX2.clip = sfx;
			audioSourceSFX2.Play ();
		}
	}

	public void PlayRadioUploadSFX()
	{
		if (radioUploading == false){
			audioSourceUploadSFX.clip = uploadRadioSFX;
			audioSourceUploadSFX.Play();
			radioUploading = true;
		}
	}

	public void StopRadioUploadSFX()
	{
		if (radioUploading == true)
		{
			audioSourceUploadSFX.Stop ();
			radioUploading = false;
		}
	}

	public void PlayerRunning()
	{
		if (!playerMoving)
		{
			audioSourcePlayerSFX.clip = footStepsSFX;
		//	audioSourcePlayerSFX.Play ();
			playerMoving = true;
		}
	}

	public void PlayerBushRustle()
	{
		if (!playerMoving)
		{
			audioSourcePlayerSFX.clip = bushRustleSFX;
		//	audioSourcePlayerSFX.Play ();
			playerMoving = true;
		}
	}

	public void StopPlayerFootsteps()
	{
		audioSourcePlayerSFX.Stop ();
		playerMoving = false;
	}

	public void PlayActionMusic()
	{
		audioSourceMusic.clip = actionMusic;
		audioSourceMusic.Play ();
	}

	/// <summary>
	/// Adjusts the music vol. 0-1
	/// </summary>
	/// <param name="v">V.</param>
	public void AdjustMusicVol(float v)
	{
		audioSourceMusic.volume = v;
	}

	/// <summary>
	/// Adjusts the SFX vol. 0-1
	/// </summary>
	/// <param name="v">V.</param>
	public void AdjustSFXVol(float v)
	{
		audioSourceSFX.volume = v;
	}

	public void MuteMusic(bool b)
	{
		audioSourceMusic.enabled = b;
	}

	public void MuteSFX(bool b)
	{
		audioSourceSFX.enabled = b;
	}
}
