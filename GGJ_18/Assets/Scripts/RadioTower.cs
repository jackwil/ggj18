﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(SphereCollider))]
public class RadioTower : MonoBehaviour {

	GameController GC;
	SonarSphere radioSonarSphere;
	GameObject mainChar;
	AudioController audioController;
	MainCharController mc;
	List<EnemyController> enemiesInGame = new List<EnemyController>();
	List<EnemyController> enemiesInRange = new List<EnemyController>();

	public GameObject towerMesh;
	public GameObject particleFX;
	public Image chargeBarUI;
	public float playerProximityRange;
	public float maxRadius;
	public float chargeTime;

	float chargeAmount;
	float chargeRange;
	bool isCharging;
	bool isDiscovered;
	bool activated;

	void Start(){
		mainChar = GameObject.Find ("MainChar");
		audioController = AudioController.FindObjectOfType (typeof(AudioController)) as AudioController;
		mc = mainChar.GetComponent<MainCharController> ();
		GC = GameObject.Find ("GameController").GetComponent<GameController> ();
		chargeBarUI = GameObject.Find ("PowerBar").GetComponent<Image> ();

		towerMesh.SetActive (false);
		particleFX.SetActive (false);
		chargeBarUI.transform.parent.gameObject.SetActive (false);

		GameObject s = GameObject.Instantiate (Resources.Load ("SonarSphere")) as GameObject;
		radioSonarSphere = s.GetComponent<SonarSphere> ();
		radioSonarSphere.SetOwner (this.gameObject);
		radioSonarSphere.transform.position = this.transform.position + Vector3.up * 0.5f;
		radioSonarSphere.GetComponent<SphereCollider> ().enabled = false;
		radioSonarSphere.gameObject.SetActive (false);

		enemiesInGame.AddRange (GameObject.FindObjectsOfType (typeof(EnemyController)) as EnemyController[]);
	}

	void Update()
	{
		UpdateEnemiesInRange ();
		//test
		if (Input.GetKeyDown (KeyCode.KeypadEnter))
			BeginChargeUp ();

		if (PlayerWithinRangeAndCharging ()) {
			ChargePower (Time.deltaTime / chargeTime);
			audioController.PlayRadioUploadSFX ();
		}

		mc.EnableUploadFX (PlayerWithinRangeAndCharging ());
		
	}

	private bool PlayerWithinRangeAndCharging()
	{
		if (isDiscovered && isCharging) {
			float distanceToPlayer = Vector3.Distance (this.transform.position, mainChar.transform.position);

			if (distanceToPlayer < playerProximityRange)
				return true;
			else
			{
				audioController.StopRadioUploadSFX ();
				return false;
			}
		} else
			return false;
	}

	public void TowerDiscovered()
	{
		towerMesh.SetActive (true);
		chargeBarUI.transform.parent.gameObject.SetActive (true);
		isDiscovered = true;
	}

	public void BeginChargeUp() // Called from PlayerInput
	{
		radioSonarSphere.gameObject.SetActive (true);
		particleFX.SetActive (true);
		isCharging = true;

		if (!activated)
			audioController.PlayActionMusic ();
		
		activated = true;
	}

	void ChargePower(float t) // Runs in Update
	{
		chargeAmount += t;

		chargeRange = Mathf.Lerp (0, maxRadius, chargeAmount);
		radioSonarSphere.transform.localScale = Vector3.Lerp (Vector3.zero, Vector3.one * maxRadius, chargeAmount) * 2;
		chargeBarUI.fillAmount = chargeAmount;

		if (chargeRange > playerProximityRange) {
			playerProximityRange = chargeRange;
		} 

		if (chargeAmount > 1)
			ChargeComplete ();
	}

	void ChargeComplete()
	{
		GC.PlayerVictory ();
	}

	void UpdateEnemiesInRange()
	{
		foreach (EnemyController enemy in enemiesInGame) {
			float distanceToEnemy = Vector3.Distance (this.transform.position, enemy.transform.position);

			if (distanceToEnemy < chargeRange) {
				enemy.EnemyRadioDiscovered ();
				enemy.PlayerDiscovered ();
			} else if (distanceToEnemy > chargeRange && activated) {
				enemy.EnemyRadioLost ();
			}

		}
	}
}
