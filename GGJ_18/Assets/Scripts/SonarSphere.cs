﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonarSphere : MonoBehaviour {

	GameObject mainChar;
	GameObject radioTower;
	public GameObject owner;
	public GameObject dynamicTorus;
	public float innerRingSpeed = 1;

	List<EnemyController> enemyList = new List<EnemyController>();
	List<EnemyController> enemiesInRangeList = new List<EnemyController> ();

	List<Breadcrumb> breadcrumbList = new List<Breadcrumb>();
	List<Breadcrumb> breadcrumbsInRangeList = new List<Breadcrumb>();

	public float sonarDuration;
	float storedDuration; // Read only
	public bool sonarActive;
	public float sonarRange;

	bool sonarPreviouslyActive;
	bool towerDiscovered;
	bool sphereGrowing;


	void Start()
	{
		mainChar = GameObject.Find ("MainChar");
		radioTower = GameObject.Find ("RadioTower");
		enemyList.AddRange (GameObject.FindObjectsOfType (typeof(EnemyController)) as EnemyController[]);
		breadcrumbList.AddRange (GameObject.FindObjectsOfType (typeof(Breadcrumb)) as Breadcrumb[]);
	}

	void Update() {
		if (sonarActive && sonarDuration > 0) {
			sonarDuration -= Time.deltaTime;
			UpdateEnemyAlerts ();
			ShowBreadcrumbs ();
			GrowSonicSphere ();
			GrowInnerPulse ();
		} else if (sonarActive && sonarDuration < 0) {
			DeActivateSonar ();
		}

	}

	public void UpdateEnemyAlerts() // Fired while sonar active
	{
		float distToPlayer = Vector3.Distance (this.transform.position, mainChar.transform.position);

		foreach (EnemyController enemy in enemyList) {
			if (Vector3.Distance (this.transform.position, enemy.transform.position) < sonarRange) // Find enemies within sonar range
				enemiesInRangeList.Add (enemy);
		}


		foreach (EnemyController enemyInSonar in enemiesInRangeList)
		{
			enemyInSonar.isInsideSonar = true;
			enemyInSonar.EnemyDiscovered ();

			// if player within sonar field
			if (distToPlayer < sonarRange)
				enemyInSonar.PlayerDiscovered ();

			// if player has escaped sonar field
			else if (distToPlayer > sonarRange)
				enemyInSonar.PlayerLost ();
		}
	}

	void CheckForTower()
	{
		if (owner.gameObject != radioTower) {
			float distToTower = Vector3.Distance (this.transform.position, radioTower.transform.position);
			if (distToTower < sonarRange && !towerDiscovered){
				towerDiscovered = true;
				radioTower.GetComponent<RadioTower> ().TowerDiscovered ();
			}
		}
	}

	void ShowBreadcrumbs()
	{
		foreach (Breadcrumb b in breadcrumbList)
			if (Vector3.Distance (this.transform.position, b.transform.position) < sonarRange)
				breadcrumbsInRangeList.Add (b);

		foreach (Breadcrumb bSonar in breadcrumbsInRangeList)
			bSonar.ActivateCrumb ();
	}

	public void ActivateSonar(Vector3 pos, float duration, float range)
	{
		this.gameObject.SetActive (true);
		this.transform.position = pos;
		//this.transform.localScale = Vector3.one * range * 2;
		sonarDuration = duration;
		sonarRange = range;
		sonarActive = true;
		sonarPreviouslyActive = true;
		sphereGrowing = true;

		CheckForTower ();
	}

	void DeActivateSonar() // Called via Update
	{
		//Shrink sphere
		if (this.transform.localScale.x > 0.1f) {
			this.transform.localScale -= Vector3.one * Time.deltaTime * 80;
			return;
		} else {
			
			sonarActive = false;

			enemiesInRangeList.Clear ();

			if (sonarPreviouslyActive) {
				foreach (EnemyController enemy in enemyList) {
					enemy.isInsideSonar = false;
					enemy.PlayerLost ();
				}
			}

			this.gameObject.transform.localScale = Vector3.one * 0.1f;
			this.gameObject.SetActive (false);
		}
	}

	public void SetOwner(GameObject go)
	{
		owner = go;
	}

	void GrowSonicSphere() // Runs via Update
	{
		if (sphereGrowing) {
			if (this.transform.localScale.x < sonarRange * 2)
				this.transform.localScale += Vector3.one * Time.deltaTime * 80;
			else if (this.transform.localScale.x > sonarRange * 2)
				sphereGrowing = false;
		}
	}



	void GrowInnerPulse() // Runs via update
	{

		if (dynamicTorus.transform.localScale.x < 1)
			dynamicTorus.transform.localScale += Vector3.one * Time.deltaTime * innerRingSpeed;
		else //if (dynamicTorus.transform.localScale.x > sonarRange)
			dynamicTorus.transform.localScale = Vector3.zero;
	}

}
