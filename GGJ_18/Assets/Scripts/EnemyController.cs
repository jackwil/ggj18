﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour {

	public float viewRange;
	public float moveSpeed;
	public GameObject sonarParticleFX;
	public GameObject sparksParticleFX;
	public GameObject alertIcon;
	public GameObject robotMeshGroup;
	public Animator myAnimator;
	public float signalLifeTime;

	NavMeshAgent myNavMesh;
	MainCharController mainChar;
	AudioSource myAudio;
	Vector3 targetPos;
	bool isChasing;
	[HideInInspector]
	public bool isInsideSonar;

	bool discovered;
	bool radioDiscovered;
	bool playerSighted;

	bool isLocked; // Unlocked when Enemy is Hidden. See CoRoutines
	float lockedStateTimer;

	bool animDiscoveredPlayer;
	bool animAlerted;


	void Start()
	{
		// Cheeky
		viewRange = 4;


		myNavMesh = GetComponent<NavMeshAgent> ();
		mainChar = GameObject.Find ("MainChar").GetComponent<MainCharController> ();
		myAudio = GetComponent<AudioSource> ();

		myNavMesh.speed = moveSpeed;
		targetPos = this.transform.position;

		robotMeshGroup.SetActive (false);
		sonarParticleFX.SetActive (false);
		alertIcon.SetActive (false);

	}

	void Update()
	{
		MoveToPos ();
		ProximitySense ();
		if (myAnimator.gameObject.activeSelf)
			ANIM_ChasingPlayer (isChasing);

		if (isLocked)
			lockedStateTimer -= Time.deltaTime;
		
		if (lockedStateTimer < 0)
		{
			lockedStateTimer = 0.5f; // How long to lock the enemy in 'Discovered' time even if the player evades
			isLocked = false;
		}

	}

	void MoveToPos()
	{
		myNavMesh.SetDestination (GetTargetPos());
	}

	Vector3 GetTargetPos(){
		if (isChasing == true){
			targetPos = mainChar.transform.position;
			return targetPos;
		} else{
			return targetPos;
		}
	}

	void ProximitySense()
	{
		float distToPlayer = Vector3.Distance (this.transform.position, mainChar.transform.position);
		if (distToPlayer < viewRange) {
			isChasing = true;
			EnemyDiscovered ();
			PlayerDiscovered ();
		} else {
			isChasing = false;
			if (isInsideSonar == false)
				EnemyHidden ();
		}

	}

	public void PlayerDiscovered() // Lock
	{
		if (isLocked)
			return;

		if (!mainChar.isHiding) {
			isLocked = true;
			isChasing = true;
			//playerSighted = true;
			sparksParticleFX.SetActive (true);
			myAudio.Play ();
			ANIM_DiscoverPlayer ();
		}
	}

	public void PlayerLost()
	{
		if (!isLocked)
		{
			isChasing = false;
			//playerSighted = false;
			sparksParticleFX.SetActive (false);
		}
	}

	public void EnemyDiscovered() // aka 'Alerted'
	{
		if (!radioDiscovered) {
			robotMeshGroup.SetActive (true);
			sonarParticleFX.SetActive (false);
			StartCoroutine (ActivateAlertIcon ());
			discovered = true;

			if (mainChar.isHiding)
				ANIM_Alert ();
		}
	}

	public void EnemyRadioDiscovered() // Called from Radio Tower. Not 'Alerted'
	{
		if (!discovered && !isLocked)
		{
			isLocked = true;
			robotMeshGroup.SetActive (true);
			radioDiscovered = true;
			discovered = true;
			StartCoroutine (ActivateAlertIcon ());
		}
	}

	public void EnemyRadioLost() // Called from Radio Tower
	{
		if (!isLocked)
		{
			radioDiscovered = false;
			EnemyHidden ();
		}
	}

	public void EnemyHidden()
	{
		if (!radioDiscovered && !isLocked) {
			robotMeshGroup.SetActive (false);
			if (discovered == true) {
				sonarParticleFX.SetActive (true);
				ANIM_StopAnims ();
				StartCoroutine (HideSignal ());
			}
		}
	}

	IEnumerator HideSignal()
	{
		discovered = false;
		myAudio.Pause ();
		//isLocked = false;
		yield return new WaitForSecondsRealtime (signalLifeTime);
		sonarParticleFX.SetActive (false);
	}

	IEnumerator ActivateAlertIcon()
	{
		if (!isLocked)
		{
			alertIcon.SetActive (true);
			yield return new WaitForSecondsRealtime (1);
			alertIcon.SetActive (false);
		}
	}



	#region ANIMATION

	// Note:
	// Discover player takes precedence over alert

	void ANIM_DiscoverPlayer()
	{
		if (!animDiscoveredPlayer)
		{
			myAnimator.SetTrigger ("anim_DiscoverPlayer");
			animDiscoveredPlayer = true;
		}
	}

	void ANIM_ChasingPlayer(bool b)
	{
		myAnimator.SetBool ("chasingPlayer", b);
	}

	void ANIM_Alert()
	{
		if (!animAlerted && !animDiscoveredPlayer)
		{
			myAnimator.SetTrigger ("anim_Alerted");
			animAlerted = true;
		}
	}

	void ANIM_StopAnims()
	{
		animDiscoveredPlayer = false;
		animAlerted = false;
		myAnimator.ResetTrigger ("anim_DiscoverPlayer");
		myAnimator.ResetTrigger ("anim_Alerted");
	}

	#endregion
}
