﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	Transform mainChar;

	void Start()
	{
		mainChar = GameObject.Find ("MainChar").transform;
	}

	void Update()
	{
		this.transform.position = mainChar.position;
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.cyan;
		Gizmos.DrawLine (this.transform.position, this.transform.GetChild (0).transform.position);
	}
}
