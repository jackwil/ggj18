﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breadcrumb : MonoBehaviour {

	public GameObject particleFX;
	public GameObject myWaypoint;

	void Start()
	{
		GetComponent<MeshRenderer> ().enabled = false;
		particleFX.SetActive (false);
	}


	public void ActivateCrumb()
	{
		particleFX.SetActive (true);
		myWaypoint.SetActive (true);
	}

	public void HideMarker() // Called from Breadcrumb Controller
	{
		myWaypoint.SetActive (false);
	}
}
