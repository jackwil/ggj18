﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonarRadio : MonoBehaviour {

	MainCharController mainChar;
	AudioController audioController;

	public float secondsToOverCharge;
	public float lvl1Range;
	public float lvl2Range;
	public float lvl3Range;
	public float lvl4Range;
	public float lvl5Range;
	public float overChargeRange;

	public List<GameObject> chargeImages = new List<GameObject>();
	public GameObject sonarDeviceObject;
	public List<ParticleSystem> overchargePFXs = new List<ParticleSystem> ();
	Vector3 sonarDeviceOrigin;
	bool shakeRadio;
	float shakeRadioDuration = 0.2f;

	//[HideInInspector]
	public int currentChargeLvl;
	int prevChargeLvl;

	float chargeRange;
	float chargedTime;
	float secondsBetweenLevels;

	float lvl1StartTime;
	float lvl2StartTime;
	float lvl3StartTime;
	float lvl4StartTime;
	float lvl5StartTime;

	void Start()
	{
		secondsBetweenLevels = secondsToOverCharge / 6; // 5 charge phases, 6 = Auto Fire
		mainChar = GameObject.Find("MainChar").GetComponent<MainCharController>();
		audioController = GameObject.Find ("AudioController").GetComponent<AudioController> ();
		sonarDeviceOrigin = sonarDeviceObject.transform.localPosition;

		lvl1StartTime = secondsBetweenLevels;
		lvl2StartTime = secondsBetweenLevels * 2;
		lvl3StartTime = secondsBetweenLevels * 3;
		lvl4StartTime = secondsBetweenLevels * 4;
		lvl5StartTime = secondsBetweenLevels * 5;
	}

	void Update()
	{
		chargedTime += Time.deltaTime;


		if (prevChargeLvl != currentChargeLvl)
			shakeRadio = true;

		if (shakeRadio)
		{
			ShakeRadio ();
			shakeRadioDuration -= Time.deltaTime;

			if (shakeRadioDuration < 0)
			{
				shakeRadio = false;

				if (prevChargeLvl != 5)
					shakeRadioDuration = 0.2f;
				else
					shakeRadioDuration = 1f;

			}
			


		}

		SetChargeLevel (chargedTime);
	}


	void SetChargeLevel(float t)
	{
		prevChargeLvl = currentChargeLvl;
		if (t < lvl1StartTime)
			currentChargeLvl = 0;
		if (t > lvl1StartTime && t < lvl2StartTime)
			currentChargeLvl = 1;
		if (t > lvl2StartTime && t < lvl3StartTime)
			currentChargeLvl = 2;
		if (t > lvl3StartTime && t < lvl4StartTime)
			currentChargeLvl = 3;
		if (t > lvl4StartTime && t < lvl5StartTime)
			currentChargeLvl = 4;
		if (t > lvl5StartTime && t > lvl5StartTime)
			currentChargeLvl = 5;
		if (t > secondsToOverCharge)
			currentChargeLvl = 6;



		switch (currentChargeLvl) {
		case 1:
			chargeRange = lvl1Range;
			break;
		case 2:
			chargeRange = lvl2Range;
			break;
		case 3: 
			chargeRange = lvl3Range;
			break;
		case 4:
			chargeRange = lvl4Range;
			break;
		case 5:
			chargeRange = lvl5Range;
			break;
		case 6:
			chargeRange = overChargeRange; 
			ActivateSonar();
			chargedTime = 0;
			break;
		}



		UpdateChargeMeter (currentChargeLvl);
			
	}

	void UpdateChargeMeter(int lvl)
	{
		if (currentChargeLvl < 6 && currentChargeLvl > 0)
		chargeImages [currentChargeLvl - 1].SetActive (true);

		if (currentChargeLvl == 0)
			foreach (GameObject image in chargeImages)
				image.SetActive (false);
	}

	public void ActivateSonar() // Called from Button UI
	{
		audioController.PlaySFX (audioController.useSonarSFX);
		if (currentChargeLvl > 0) {
			chargedTime = 0;
			mainChar.FireSonar (chargeRange);
		}

		if (currentChargeLvl == 6)
			audioController.PlaySFX (audioController.sonarOverchargeSFX);

		// Janky method of stopping the player from moving when button is touched.
		mainChar.SetDestinationPos(mainChar.prevDestination);
	}

	public void DEV_ActivateSonar(int DEV_chargeLevel)
	{
		mainChar.FireSonar (DEV_chargeLevel);
		chargedTime = 0;
	}

	void ShakeRadio()
	{
		int chargeMultipler;

		if (prevChargeLvl == 0)
			chargeMultipler = 6;
		else
			chargeMultipler = prevChargeLvl;

		int mx = (int)Mathf.Round (Random.Range (-1, 1));
		int my = (int)Mathf.Round (Random.Range (-1, 1));
		sonarDeviceObject.transform.localPosition = new Vector3 (sonarDeviceOrigin.x + mx * chargeMultipler * 2, sonarDeviceOrigin.y + my * chargeMultipler * 2, sonarDeviceOrigin.z);

		if (prevChargeLvl == 6)
			foreach (ParticleSystem pfx in overchargePFXs)
				pfx.Play ();
	}
}
