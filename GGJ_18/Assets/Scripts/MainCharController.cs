﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent (typeof(NavMeshAgent))]
public class MainCharController : MonoBehaviour {

	GameController GC;
	AudioController audioController;
	public Animator myAnims;

	public float speed;
	//public float sonarRange = 5;
	public float sonarDuration = 3;
	NavMeshAgent myNavAgent;
	Vector3 destinationPos;
	SonarSphere sonarSphere;
	Vector3 radioTower;

	public ParticleSystem grassParticleFX;
	public ParticleSystem uploadParticleFX;

	public bool isHiding;

	[HideInInspector]
	public Vector3 prevDestination; // Used for work-around of sonar device button moving the player

	// Use this for initialization
	void Start () {
		EnableUploadFX (false);
		GC = GameObject.Find ("GameController").GetComponent<GameController> ();
		audioController = GC.GetComponentInChildren<AudioController> ();
		radioTower = GameObject.Find ("RadioTower").transform.position;
		myNavAgent = GetComponent<NavMeshAgent> ();
		myNavAgent.speed = speed;
		destinationPos = this.transform.position;

		GameObject s = GameObject.Instantiate (Resources.Load ("SonarSphere")) as GameObject;
		sonarSphere = s.GetComponent<SonarSphere> ();
		sonarSphere.SetOwner (this.gameObject);
		sonarSphere.transform.position = this.transform.position;
		sonarSphere.GetComponent<SphereCollider> ().enabled = false;
		sonarSphere.gameObject.SetActive (false);

	}

	// Update is called once per frame
	void Update () {
		Movement ();

		if (myNavAgent.velocity.magnitude > 0.5f && isHiding)
		{
			grassParticleFX.Play ();
			audioController.PlayerBushRustle ();
		} else if (myNavAgent.velocity.magnitude > 0.5f && !isHiding)
		{
			grassParticleFX.Stop ();
			audioController.PlayerRunning ();
		} else if (myNavAgent.velocity.magnitude < 0.2f)
			audioController.StopPlayerFootsteps ();


		uploadParticleFX.transform.LookAt (radioTower);
	}

	void Movement()
	{
		float distToDest = Vector3.Distance (this.transform.position, destinationPos);

		if (distToDest > 0.2f && destinationPos != null)
			myNavAgent.SetDestination (destinationPos);

		//Animation
		if (myNavAgent.velocity.magnitude > 0.2f)
		{
			myAnims.SetBool ("isRunning", true);


		}
		else if (myNavAgent.velocity.magnitude < 0.2f)
			myAnims.SetBool ("isRunning", false);
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.CompareTag ("Enemy")) {
			myNavAgent.speed = 0;
			GC.PlayerDefeated ();
		}

		if (col.CompareTag ("StealthZone"))
			isHiding = true;
	}

	void OnTriggerExit(Collider col)
	{
		if (col.CompareTag ("StealthZone"))
			isHiding = false;
	}


	public void SetDestinationPos(Vector3 pos)
	{
		destinationPos = pos;
		prevDestination = pos;
	}

	IEnumerator EnableNavAgent()
	{
		yield return new WaitForSeconds (0.1f);
		myNavAgent.enabled = true;
	}

	public void FireSonar (float sonarRange) // Called from SonarRadio
	{
		sonarSphere.ActivateSonar (this.transform.position, sonarDuration, sonarRange);
	}

	public void EnableUploadFX(bool b) // Called from Radio
	{
		uploadParticleFX.gameObject.SetActive (b);
	}
}
