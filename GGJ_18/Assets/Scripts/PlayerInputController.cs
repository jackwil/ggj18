﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerInputController : MonoBehaviour {

	GameController GC;
	MainCharController mainChar;
	SonarRadio sonarRadio;
	Button sonarButton;

	public GameObject pointMarker;
	Vector3 markerTargetPos;
	public float markerSpeed = 1;
	public float torusSize = 1;
	public float markerStayTime = 1;
	float storedMarkerStayTime;
	public bool enableDevControls;

	private int fingerID = -1;
	bool placingMarker;

	private void Awake()
	{
		#if !UNITY_EDITOR
		fingerID = 0;
		#endif
	}

	void Start()
	{
		GC = GameObject.Find ("GameController").GetComponent<GameController> ();
		mainChar = GameObject.Find ("MainChar").GetComponent<MainCharController> ();
		if (enableDevControls) {
			Debug.LogWarning ("Dev controls active");
			sonarRadio = GameObject.FindObjectOfType (typeof(SonarRadio)) as SonarRadio;
		}
		pointMarker.transform.SetParent (null);
		pointMarker.SetActive (false);

		storedMarkerStayTime = markerStayTime;
	}

	void Update()
	{
		if (EventSystem.current.IsPointerOverGameObject (fingerID)) {
			return;
		}


		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) {
			SetTouchPos (Input.GetTouch (0).position);
		}

		// Mouse input
		if (Input.GetMouseButtonDown(0)){
			SetTouchPos (Input.mousePosition);
		}

		MoveMarker ();

		//DEV CONTROLS
		if (Input.GetKey (KeyCode.Alpha1))
			sonarRadio.DEV_ActivateSonar (1);
		if (Input.GetKey (KeyCode.Alpha2))
			sonarRadio.DEV_ActivateSonar (2);
		if (Input.GetKey (KeyCode.Alpha3))
			sonarRadio.DEV_ActivateSonar (3);
		if (Input.GetKey (KeyCode.Alpha4))
			sonarRadio.DEV_ActivateSonar (4);
		if (Input.GetKey (KeyCode.Alpha5))
			sonarRadio.DEV_ActivateSonar (5);
		if (Input.GetKey (KeyCode.Alpha6))
			sonarRadio.DEV_ActivateSonar (6);


	}

	private bool IsPointerOverUIObject() {
		// Referencing this code for GraphicRaycaster https://gist.github.com/stramit/ead7ca1f432f3c0f181f
		// the ray cast appears to require only eventData.position.
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
		return results.Count > 0;
	}

	void SetTouchPos(Vector2 touchPos) // Runs in Update
	{

		if (GC.currentGameState == GameController.gameState.active) {

			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (touchPos);

			if (Physics.Raycast (ray, out hit)) {
				if (hit.collider.CompareTag ("NavMesh") && !IsPointerOverUIObject()) {
					mainChar.SetDestinationPos (hit.point); // Actual part where touch pos tells capsule where to move
					PlaceMarker(hit.point);
				} else if (hit.collider.CompareTag ("RadioTower")) {
					hit.collider.GetComponentInParent<RadioTower> ().BeginChargeUp ();
				}
			}
		}
	}

	void PlaceMarker(Vector3 pos)
	{
		placingMarker = true;
		pointMarker.SetActive (true);
		pointMarker.transform.position = new Vector3 (pos.x, 10, pos.z);
		markerTargetPos = pos;

		markerStayTime = storedMarkerStayTime;
	}

	void MoveMarker() // Via Update
	{
		if (placingMarker) {
			pointMarker.transform.position = Vector3.Lerp (pointMarker.transform.position, markerTargetPos, Time.deltaTime * markerSpeed);

			markerStayTime -= Time.deltaTime;

			if (markerStayTime < 0) {
				pointMarker.SetActive (false);
				placingMarker = false;
			}
		}
	}
}
