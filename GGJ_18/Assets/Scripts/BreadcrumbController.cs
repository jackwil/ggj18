﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreadcrumbController : MonoBehaviour {

	[Header ("Do not fill manually")]
	public List<Transform> allMarkers = new List<Transform> ();



	void OnDrawGizmos()
	{
		Gizmos.color = Color.magenta;

		foreach (Transform t in this.transform)
		{
			int nextWaypoint = t.GetSiblingIndex () + 1;

			if (nextWaypoint < this.transform.childCount)
				Gizmos.DrawLine (t.position, this.transform.GetChild (nextWaypoint).transform.position);
		}
	}

	void Start()
	{
		GameObject[] markersFound = GameObject.FindGameObjectsWithTag ("MarkerPointer");
		foreach (GameObject m in markersFound)
			allMarkers.Add (m.transform);

		SetLookAtTargets ();
	}

	void SetLookAtTargets()
	{
		for (int i = 0; i < this.transform.childCount - 1; i++)
		{
			Transform marker = this.transform.GetChild (i).Find("WaypointContainer");
			marker.LookAt (this.transform.GetChild(i+1).transform.position + Vector3.up);
		}

		Transform finalMarker = this.transform.GetChild (this.transform.childCount - 1).Find ("WaypointContainer");
		finalMarker.LookAt (GameObject.Find ("RadioTower").transform.position);

		HideAllMarkers ();
	}

	void HideAllMarkers()
	{
		foreach (Transform marker in allMarkers)
			marker.GetComponentInParent<Breadcrumb> ().HideMarker ();
	}
}
